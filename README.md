# An introduction to R and the tidyverse

This repo accompanies a workshop on R and the tidyverse. It's designed for the psychological researcher with little to no programming experience. The content mostly consists of what one might find in an introductory statistics class (e.g., descriptive stats, data visualization, correlation, regression, t-tests, ANOVA). It also covers an introduction to data manipulation and writing scripts.
