<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <title>An introduction to R and the tidyverse</title>
    <meta charset="utf-8" />
    <meta name="author" content="J. Winget" />
    <meta name="date" content="2019-11-18" />
    <link href="learning-r_files/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="learning-r_files/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="learning-r_files/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# An introduction to R and the tidyverse
### J. Winget
### 2019-11-18

---

#  Today's goals

+ By the end of the workshop, you should be able to...
  1. Recognize the difference between R and RStudio
  1. Execute basic statistical analyses in R and the tidyverse
  1. Construct basic data visualizations using ggplot2
  1. Locate resources for troubleshooting and extending your R skills

???

+ 

---
#  What is R/RStudio?

+ R is a free software environment for statistical computing and graphics
  + It’s actually its own computer language, much like Python, C, C++, etc., but it’s made specifically for data analysis
+ RStudio is basically an application that interfaces with R that makes R much more flexible/powerful
  + So powerful that we won’t even scratch the surface of its capabilities in this workshop
  + It also makes R easier and more intuitive to use

.center[&lt;img src="assets/img/image7.png"&gt;]

???

+ 

---
#  Why R?

+ FREE!
+ Open source
+ Reproducibility
+ Very flexible in the types of data it can handle
+ Vast array of analytic methods
  + Power analysis, SEM, meta-analysis, quantitative text analysis, and more
+ New methods sooner
+ Incredible data viz options
+ Can also be used to create slides, research manuscripts, websites, and more!

???

+ If you already know SPSS, why bother learning R?
+ First benefit is that it's completely free and runs on all computer platforms
  + No more licenses to worry about or computer rooms to reserve
+ It's also open source, which means you can see the actual code that was written to produce your results
  + You can even modify this code for your own needs if you wish
  + Makes it easier to understand what's going on and what the program is doing
+ Using open source software is making data and analyses more available and reproducible
  + Makes it more accessible to people that might not be able to buy commercially available software
  + This increases the changes that any potential errors in your analyses will be caught and corrected
+ R is also very flexible in the type of data it can analyze
  + SPSS saves data in a specific way, and will only load data from a specific file type
  + R can handle data from SPSS files, from Excel files, from CSV files, SAS files, and hundreds of other types
  + If you can think of a type of data, there's a way to get it into R
+ There's also a ton of analytic methods you can use in R
  + Anything that can be ran in SPSS can run in R
  + But R also offers SEM, machine learning, deep learning, network analysis, and other methods
  + There are literally thousands of packages available for R that you can use for virtually any type of analytic approach
+ For this reason, R offers new methods sooner as well
  + Since people who develop new analytic methods often program in R, you'll often get access to them years before the methods are added to SPSS or SAS
+ R's graphics are extremely flexible and look SO much nicer than SPSS's graphics
  + They're also more flexible and you have much more control over how they look
+ There are other benefits too
    + Whereas SPSS can only be used for statistical analysis...R can also be used to also write research reports in it (exports to word, pdf)
    + Can even handle track changes with certain packages
    + Can create slides/presentations with it (can export to powerpoint!)
    + It can sync with other software (like a library manager) and automatically import references
    + Takes a lot of human error out of the process because results are directly imported to those other areas from R

---
#  R in industry

.center[&lt;img src="assets/img/image1.png" height=500&gt;]


???

+ Becoming universal language for data analytic &amp; data science

---
#  R in industry

.center[&lt;img src="assets/img/image2.png" height=500&gt;]


???

+ Becoming universal language for data analytic &amp; data science

---
#  R in research

.center[&lt;img src="assets/img/image3.png" height=500&gt;]

???

+ Becoming universal language for data analytic &amp; data science

---
#  R in research

.center[&lt;img src="assets/img/image4.png" height=500&gt;]

???

+ Becoming universal language for data analytic &amp; data science

---
#  The R-chitecture

+ R exists as a base package with a reasonable amount of functionality
+ However, the beauty of R is that it can be expanded by downloading packages that add specific functionality to the program 
+ Packages are stored in a central location known as CRAN (Comprehensive R Archive Network)

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image8.png"&gt;]

???

+ 

---
#  Using RStudio

+ Every action (i.e., command) you want to take in RStudio needs to be entered in the console or executed from a script file
  + It may take some time getting used to this


+ Commands in R are made up of 2 parts
  + Objects
  + Functions

???

+ Unlike other statistical software programs like Excel or SPSS that provide point-and-click interfaces, R is an interpreted language
+ This means you have to type in commands written in R code
+ In other words, you have to code/program in R
+ You don't need to be a seasoned coder/computer programmer to use R, there is still a set of basic programming concepts that R users need to understand
+ 

---
#  Using RStudio

+ Object
  + Anything created in R
  + Variable, collection of variables, a statistical model, etc.
+ Function
  + Things you do in R to create objects or obtain results


+ The general form of a command is
  + Object &lt;- function
  + This reads as “object is created from function”

???

+ 

---
#  Using RStudio

+ Example:


```r
rolling_stones &lt;- c("Mick", "Keith", "Brian", "Bill", "Charlie")
```

+ rolling_stones = **object**
+ c("Mick", "Keith", "Brian", "Bill", "Charlie") = **function**


+ Creates object called “rolling_stones” with is made up of the original band members’ names
  + Used the **c** (concentrate) function, which groups things together
  + Written each members name, and by enclosing them in c(), we bind them into a single object
  + Entering this into the console and hitting enter creates the object and stores it in memory for later use

???

+ 

---
#  Using RStudio

+ Using objects
  + Type name of object into console to display its contents
  + R is case-sensitive, so if you used all lowercase, be consistent

&lt;br&gt;
&lt;br&gt;

```r
rolling_stones
```

```
## [1] "Mick"    "Keith"   "Brian"   "Bill"    "Charlie"
```

???

+ 

---
#  Scripts

+ While you can execute commands from the console, it’s better to use the R editor and execute from there
+ A document of commands written in the editor is known as a script


+ Benefits
  + You can save the script and rerun the exact same analyses at a later date
      + Fast rerunning of analyses, reproducible 
  + You can modify scripts for similar future analyses
  + When you receive an error from your code (and you will), you won’t have to rewrite everything in the console
      + Edit the code without having to rewrite it

???

+ 

---
#  Other basic programming concepts

+ Data types
  + Integers (whole numbers)
  + Numberics (numbers that allow decimals)
  + Logicals (T/F values)
  + Characters (letter strings)
  + Factors (categorical values)
+ Data frames (aka, tibbles in the tidyverse)
+ Conditionals
  + Testing for equality
      + e.g., to test if 2 + 1 is equal to 3, use == 
      + Don't use = 
  + Boolean algebra
      + e.g., less than (&lt;), less than or equal to (&lt;=), not equal to (!=)
  + Logical operators
      + e.g., for "and", use &amp;
      + e.g., for "or", use |
  
???

+

---
#  The tidyverse

+ Many ways to approach statistics with R
+ We’re going to use the tidyverse
  + It’s a **collection** of packages that work with each other to make data analysis/graphing easy

.center[&lt;img src="assets/img/image11.png"&gt;]

???

+ Read slide first...
+ So R is like a new mobile phone: while it has a certain amount of features when you use it for the first time, it doesn’t have everything
+ R packages are like the apps you can download onto your phone from Apple’s App Store or Android’s Google Play
+ 
+ It’s a favorite in the industry
+ It makes many complex approaches in base R much simpler
+ Tends to be more intuitive, especially if you’re new to R/RStudio
+ Easier for others to read code

---
#  The tidyverse vs. base R

&lt;br&gt;
&lt;br&gt;
Base R:


```r
chicago_crime_2018 &lt;- chicago_crime_2018[, c("type_of_crime", "count")]
```

tidyverse:


```r
chicago_crime_2018 %&gt;% 
  select(type_of_crime, count)
```


???

+ With base R, you need to know more about programming
+ Whereas the tidyverse is very user-friendly...especially for beginners/novices

---
#  The tidy format

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image10.png"&gt;]

???

+ The tidyverse is based on the tidy format philosophy...which is what we're all accustomed to with SPSS
+ There are three interrelated rules which make a dataset tidy:
  + Each variable must have its own column
  + Each observation must have its own row
  + Each value must have its own cell

---
#  Load the tidyverse

+ To use the packages, you must load them


```r
library(tidyverse)
```

+ You must do this **every time** you open a new RStudio session
+ Common practice to load packages at beginning of script

???

+ 

---
#  Let's start coding!

.center[&lt;img src="assets/cat.gif" alt="Loading" title="Loading" width=65%/&gt;]

???

+ 
---
#  Troubleshooting

1. Read the documentation by adding ? in front of the function
  + e.g., ```?read_csv```
1. Google the error message
  + Remove any terms specific to your problem (e.g., object names)
1. Smart search on Google, StackOverflow, or the RStudio community
  + Pick a few words/tags and use only bare minimum to get results (adding "tidyverse" as a tag often helps)
1. Make the problem as small as possible...try to isolate what's happening
1. Watch this webinar on reproducible examples: https://t.co/FALZlCwqOw
1. Ask question on twitter and use #rstats
  + Incredibly helpful and supportive community
1. Ask a colleague/friend
1. Ask question on StackOverflow or in RStudio community

.right[&lt;img src="assets/img/image12.jpeg" height=200&gt;]

???

+ 
---
# R-esources

.pull-left[
+ [R for Data Science](https://r4ds.had.co.nz/) 
  + *by Hadley Wickham*
+ [R for Psychological Science](https://psyr.org/)
  + *by Danielle Navarro*
+ [Learning Statistics with R](https://learningstatisticswithr.com/)
  + *by Danielle Navarro*
+ [STAT 545](https://stat545.com/) 
  + *by Jenny Bryan*
+ [R Cookbook](https://rc2e.com/) 
  + *by James (JD) Long &amp; Paul Teetor*
+ [Common statistical tests are linear models](https://lindeloev.github.io/tests-as-linear/)
  + *by Jonas Kristoffer Lindeløv*
]

.pull-right[
+ [How Do I...](https://smach.github.io/R4JournalismBook/HowDoI.html) 
  + *by Sharon Machlis*
+ [R Bootcamp](https://r-bootcamp.netlify.com/) 
  + *by Ted Laderas &amp; Jessica Minnier*
+ [Getting used to R, RStudio, and R Markdown](https://bookdown.org/chesterismay/rbasics/) 
  + *by Chester Ismay*
+ [YaRrr! The Pirate's Guide to R](https://bookdown.org/ndphillips/YaRrr/)
  + *by Nathaniel D. Phillips*
+ [LeaRing ResouRces GitLab repo](https://gitlab.com/jrwinget/leaRning-ResouRces)
  + *by me!*
]

???

+
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create();
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();</script>

<script>
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
